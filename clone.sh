#!/usr/bin/env bash

ensure_group_dirs() {
  dir=$1
  if [[ ! -d "./$dir" ]]; then
    mkdir $dir 
  fi
  echo "$dir verified"
}

clone_repo() {
  if [[ ! -d "./$2" ]]; then
    git clone https://gitlab.com/ms-experiments/check-up-tool/$1/$2
  fi
  echo "$1/$2 cloned"
}

clone_unescoped_repo() {
  if [[ ! -d "./$2" ]]; then
    git clone https://gitlab.com/ms-experiments/check-up-tool/$1
  fi
  echo "$1 cloned"
}

dirs=(
  profiling
  actions
  diseases
)

cd ..

for dir in "${dirs[@]}"; do
  ensure_group_dirs $dir
done

cd profiling

clone_repo profiling api

cd ../diseases

clone_repo diseases api
clone_repo diseases sink

cd ../actions

clone_repo actions api
clone_repo actions sink

cd ..

clone_unescoped_repo tests
