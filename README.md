# Automate

This repo offers two BASH scripts that automates the cloning process for the [Check-Up Tool][1] project.

- `clone.sh` will create the directory structure and clone the repositories;
- `set_docker.sh` will copy the `docker-compose` files for either running the development containers and the testing container.

To run the scripts, do:

```bash
bash clone.sh
bash set_docker.sh
```

> there's no predefined order to run the scripts -- they're independent

## `clone.sh`

This script scaffolds this directory structure:
- check-up-tool
  - actions
    - [api][2]
    - [sink][3]
  - automate
  - diseases
    - [api][4]
    - [sink][5]
  - profiling
    - [api][6]
  - [tests][7]

## `set_docker.sh`

This script simply copies useful `docker-compose` files to the project's root directory

- `docker-compose.yaml`: runs the development containers
- `docker-compose.test.yaml`: runs the testing container

[1]: https://gitlab.com/ms-experiments/check-up-tool
[2]: https://gitlab.com/ms-experiments/check-up-tool/actions/api
[3]: https://gitlab.com/ms-experiments/check-up-tool/actions/sink
[4]: https://gitlab.com/ms-experiments/check-up-tool/diseases/api
[5]: https://gitlab.com/ms-experiments/check-up-tool/diseases/sink
[6]: https://gitlab.com/ms-experiments/check-up-tool/profiling/api
[7]: https://gitlab.com/ms-experiments/check-up-tool/tests
